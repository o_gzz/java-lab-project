/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Client {
    int cx_id;
    String cx_name;
    String cx_lastname;
    String cx_st_address;
    int cx_pc;
    String cx_city;
    String cx_state;
    String cx_country;
    long cx_phone;
    String cx_email;
    float cx_credit_limit;

    public Client(int cx_id, String cx_name, String cx_lastname, String cx_st_address, int cx_pc, String cx_city, String cx_state, String cx_country, long cx_phone, String cx_email, float cx_credit_limit) {
        this.cx_id = cx_id;
        this.cx_name = cx_name;
        this.cx_lastname = cx_lastname;
        this.cx_st_address = cx_st_address;
        this.cx_pc = cx_pc;
        this.cx_city = cx_city;
        this.cx_state = cx_state;
        this.cx_country = cx_country;
        this.cx_phone = cx_phone;
        this.cx_email = cx_email;
        this.cx_credit_limit = cx_credit_limit;
    }

    public int getCx_id() {
        return cx_id;
    }

    public void setCx_id(int cx_id) {
        this.cx_id = cx_id;
    }

    public String getCx_name() {
        return cx_name;
    }

    public void setCx_name(String cx_name) {
        this.cx_name = cx_name;
    }

    public String getCx_lastname() {
        return cx_lastname;
    }

    public void setCx_lastname(String cx_lastname) {
        this.cx_lastname = cx_lastname;
    }

    public String getCx_st_address() {
        return cx_st_address;
    }

    public void setCx_st_address(String cx_st_address) {
        this.cx_st_address = cx_st_address;
    }

    public int getCx_pc() {
        return cx_pc;
    }

    public void setCx_pc(int cx_pc) {
        this.cx_pc = cx_pc;
    }

    public String getCx_city() {
        return cx_city;
    }

    public void setCx_city(String cx_city) {
        this.cx_city = cx_city;
    }

    public String getCx_state() {
        return cx_state;
    }

    public void setCx_state(String cx_state) {
        this.cx_state = cx_state;
    }

    public String getCx_country() {
        return cx_country;
    }

    public void setCx_country(String cx_country) {
        this.cx_country = cx_country;
    }

    public long getCx_phone() {
        return cx_phone;
    }

    public void setCx_phone(long cx_phone) {
        this.cx_phone = cx_phone;
    }

    public String getCx_email() {
        return cx_email;
    }

    public void setCx_email(String cx_email) {
        this.cx_email = cx_email;
    }

    public float getCx_credit_limit() {
        return cx_credit_limit;
    }

    public void setCx_credit_limit(float cx_credit_limit) {
        this.cx_credit_limit = cx_credit_limit;
    }
    
    
}
