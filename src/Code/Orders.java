/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

import java.util.Date;

/**
 *
 * @author QW
 */
public class Orders extends OrderItems{
    private Date order_date;
    private int cust_id;
    private String ship_mode;
    private String order_status;
    private int sales_rep_id;
    private float total;

    public Orders() {
    }

    public Orders(int order_id, Date order_date, int cust_id, String ship_mode, String order_status, int sales_rep_id, float total) {
        super(order_id);
        this.order_date = order_date;
        this.cust_id = cust_id;
        this.ship_mode = ship_mode;
        this.order_status = order_status;
        this.sales_rep_id = sales_rep_id;
        this.total = total;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public int getCust_id() {
        return cust_id;
    }

    public void setCust_id(int cust_id) {
        this.cust_id = cust_id;
    }

    public String getShip_mode() {
        return ship_mode;
    }

    public void setShip_mode(String ship_mode) {
        this.ship_mode = ship_mode;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public int getSales_rep_id() {
        return sales_rep_id;
    }

    public void setSales_rep_id(int sales_rep_id) {
        this.sales_rep_id = sales_rep_id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    
}
