/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class ProductInfo extends Inventories{
    private String category;
    private float weight;
    private int warranty;
    private int supplier_id;
    private String status;
    private String description;

    public ProductInfo() {
    }
    public ProductInfo(int product_id) {
        super(product_id);
    }
    public ProductInfo(String category, float weight, int warranty, int supplier_id, String status, String description) {
        this.category = category;
        this.weight = weight;
        this.warranty = warranty;
        this.supplier_id = supplier_id;
        this.status = status;
        this.description = description;
    }
    public ProductInfo(int product_id, String category, float weight, int warranty, int supplier_id, String status, String description) {
        super(product_id);
        this.category = category;
        this.weight = weight;
        this.warranty = warranty;
        this.supplier_id = supplier_id;
        this.status = status;
        this.description = description;
    }
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getWarranty() {
        return warranty;
    }

    public void setWarranty(int warranty) {
        this.warranty = warranty;
    }

    public int getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(int supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
