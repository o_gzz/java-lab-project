/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Warehouses {
    protected int warehouse_id;
    private String warehouse_name;
    private String location_id;
    public Warehouses(){
        
    }

    public Warehouses(int warehouse_id, String warehouse_name, String location_id) {
        this.warehouse_id = warehouse_id;
        this.warehouse_name = warehouse_name;
        this.location_id = location_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
    

    public Warehouses(int warehouse_id) {
        this.warehouse_id = warehouse_id;
    }
    
    public int getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(int warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getWarehouse_name() {
        return warehouse_name;
    }

    public void setWarehouse_name(String warehouse_name) {
        this.warehouse_name = warehouse_name;
    }

    
    
}
