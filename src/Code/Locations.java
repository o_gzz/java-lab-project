/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Locations extends Countries{
    protected int location_id;
    private String st_address;
    private int postal_code;
    private String city;
    private String state_province;
    public Locations(int region_id, String region_name) {
        super(region_id, region_name);
    }

    public Locations(int location_id, String st_address, int postal_code, String city, String state_province, int region_id, String region_name) {
        super(region_id, region_name);
        this.location_id = location_id;
        this.st_address = st_address;
        this.postal_code = postal_code;
        this.city = city;
        this.state_province = state_province;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getSt_address() {
        return st_address;
    }

    public void setSt_address(String st_address) {
        this.st_address = st_address;
    }

    public int getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(int postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }
    
}
