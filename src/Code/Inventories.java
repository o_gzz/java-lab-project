/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Inventories extends Warehouses{
    protected int product_id;
    private int quantity;
    public Inventories(){
    
    }

    public Inventories(int warehouse_id, String warehouse_name, String location_id) {
        super(warehouse_id, warehouse_name, location_id);
    }
    

    public Inventories(int product_id, int warehouse_id, String warehouse_name, String location_id) {
        super(warehouse_id, warehouse_name, location_id);
        this.product_id = product_id;
    }

    Inventories(int product_id, int warehouse_id, int quantity) {
        super(warehouse_id);
        this.product_id = product_id;
        this.quantity = quantity;
    }

    public Inventories(int product_id) {
        this.product_id = product_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    
    
}
