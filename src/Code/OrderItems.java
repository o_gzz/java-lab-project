/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class OrderItems extends ProductInfo{
    private int order_id;
    protected float unit_price;
    protected int quantity_oi;

    public OrderItems() {
    }
    public OrderItems(int order_id) {
        super(order_id);
    }


    public OrderItems(int order_id, float unit_price, int quantity_oi) {
        this.order_id = order_id;
        this.unit_price = unit_price;
        this.quantity_oi = quantity_oi;
    }

    /**
     *
     * @param order_id
     * @param product_id
     * @param unit_price
     * @param quantity_oi
     */
    public OrderItems(int order_id, int product_id, float unit_price, int quantity_oi) {
        super(order_id);
        this.order_id = order_id;
        this.unit_price = unit_price;
        this.quantity_oi = quantity_oi;
    }
    
    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public float getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(float unit_price) {
        this.unit_price = unit_price;
    }

    public int getQuantity_oi() {
        return quantity_oi;
    }

    public void setQuantity_oi(int quantity_oi) {
        this.quantity_oi = quantity_oi;
    }
    
}
